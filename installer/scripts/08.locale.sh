#!/bin/bash

echo "==>08.GENERATING LOCALE"

locale=$(jq '.locale' /installer/install_config.json | jq -r '.locale')

echo "=>COMMAND=echo $locale >> /etc/locale.gen"
echo $locale >> /etc/locale.gen

echo "=>COMMAND=locale-gen"
locale-gen
