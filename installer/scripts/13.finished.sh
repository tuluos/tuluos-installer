#!/bin/bash
echo "13.UNMOUNTING ALL PARTITIONS"
user_name=$(jq '.user' install_config.json | jq -r '.user_name')

rm -rv /mnt/installer
rm -rv /mnt/home/$user_name/.TuluOSInstaller
rm -rv /mnt/usr/share/applications/TuluOSInstaller.desktop
umount -a

echo "================INSTALLATION COMPLETE================"
