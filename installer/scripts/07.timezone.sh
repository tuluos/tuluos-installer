#!/bin/bash
echo "==>07.CHOOSING TIME ZONE"

region=$(jq '.locale' /installer/install_config.json | jq -r '.region')
zone=$(jq '.locale' /installer/install_config.json | jq -r '.zone')

echo "=>COMMAND=ln -sf /usr/share/zoneinfo/$region/$zone /etc/localtime"
ln -sf /usr/share/zoneinfo/$region/$zone /etc/localtime

echo "=>COMMAND=hwclock --systohc"
hwclock --systohc
