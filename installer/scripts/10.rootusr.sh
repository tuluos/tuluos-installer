#!/bin/bash
echo "==>10.ENTERING ROOT PASSWORD CREATING USERNAME AND PASSWORD, REMOVING LIVE USERNAME"

full_name=$(jq '.user' /installer/install_config.json | jq -r '.full_name')
user_name=$(jq '.user' /installer/install_config.json | jq -r '.user_name')
user_password=$(jq '.user' /installer/install_config.json | jq -r '.user_password')
root_password=$(jq '.user' /installer/install_config.json | jq -r '.root_password')

printf "$root_password\n$root_password" | passwd
# add quotation in full_name or it will give error
useradd -m -c "$full_name" $user_name

usermod -aG wheel,audio,video,optical,storage $user_name
printf "$user_password\n$user_password" | passwd $user_name
sed -i "s/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/g" /etc/sudoers

userdel -r live
