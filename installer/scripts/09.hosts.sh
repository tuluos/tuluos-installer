#!/bin/bash
echo "==>09.CREATING HOSTNAME AND HOSTS FILE"

hostname=$(jq '.user' /installer/install_config.json | jq -r '.host_name')

echo "=>COMMAND=rm /etc/hostname"
rm /etc/hostname
echo "=>COMMAND=rm /etc/hosts"
rm /etc/hosts

echo "=>COMMAND=echo $hostname >> /etc/hostname"
echo $hostname >> /etc/hostname

echo "=>COMMAND=check archiso for info"
echo "127.0.0.1     localhost" >> /etc/hosts
echo "::1           localhost" >> /etc/hosts
echo "127.0.1.1     $hostname.localdomain   $hostname" >> /etc/hosts
