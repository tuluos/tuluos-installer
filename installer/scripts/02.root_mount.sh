#!/bin/bash

echo "==>MOUNTING ROOT PARTITION IN /MNT"

root=$(jq '.partition' install_config.json | jq -r '.root')

echo "=>COMMAND=mount $root /mnt"

mount $root /mnt
