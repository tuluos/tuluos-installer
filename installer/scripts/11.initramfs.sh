#!/bin/bash
echo "==>11.CREATING INITRAMFS AND INITRAMFS-FALLBACK IMAGE"

rm -r /etc/mkinitcpio.conf.d

mkinitcpio -k /boot/vmlinuz-linux -g /boot/initramfs-linux.img --microcode /boot/*-ucode.img
mkinitcpio -k /boot/vmlinuz-linux -g /boot/initramfs-linux-fallback.img -S autodetect --microcode /boot/*-ucode.img
