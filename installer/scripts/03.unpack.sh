#!/bin/bash

echo "==>UNPACKING AIROOTFS AND COPYING INTEL AMD UCODE AND LINUX"

# for unpacking airootfs.sfs file in /mnt
echo "COMMAND=unsquashfs -d /mnt /run/archiso/bootmnt/arch/x86_64/airootfs.sfs"
unsquashfs -d /mnt /run/archiso/bootmnt/arch/x86_64/airootfs.sfs

# to copy intel and amd ucode in /mnt/boot folder
echo "COMMAND=cp -r /run/archiso/bootmnt/arch/boot/*-ucode.img /mnt/boot"
cp -r /run/archiso/bootmnt/arch/boot/*-ucode.img /mnt/boot

# to copy linux file in /mnt/boot folder
echo "COMMAND=cp /run/archiso/bootmnt/arch/boot/x86_64/vmlinuz-linux /mnt/boot"
cp /run/archiso/bootmnt/arch/boot/x86_64/vmlinuz-linux /mnt/boot
