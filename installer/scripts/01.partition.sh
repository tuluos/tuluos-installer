#!/bin/bash

echo "==>FORMATTING THE DISK"

# use jq -r command to get raw output or it will give error
boot=$(echo $(jq '.partition' install_config.json | jq -r '.efi'))
root=$(echo $(jq '.partition' install_config.json | jq -r '.root'))

# formatting the drives y if they are already formatted
printf "y" | mkfs.fat -F32 $boot
printf "y" | mkfs.ext4 $root
